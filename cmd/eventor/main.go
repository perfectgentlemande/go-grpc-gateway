package main

import (
	"context"
	"flag"

	"bitbucket.org/perfectgentlemande/go-grpc-gateway/internal/grpc"
	"bitbucket.org/perfectgentlemande/go-grpc-gateway/internal/http"
	"github.com/pkg/errors"

	"gopkg.in/yaml.v2"

	"log"
	"os"
)

type Config struct {
	GRPC grpc.Config `yaml:"grpc"`
	HTTP http.Config `yaml:"http"`
}

func readConfig(fname string) (*Config, error) {
	file, err := os.Open(fname)
	if err != nil {
		return nil, errors.Wrap(err, "failed to open file")
	}
	defer file.Close()

	config := &Config{}
	err = yaml.NewDecoder(file).Decode(config)
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode")
	}

	return config, nil
}

func main() {
	ctx := context.Background()

	configPath := flag.String("config", "config.yaml", "path to your config")
	flag.Parse()

	config, err := readConfig(*configPath)
	if err != nil {
		log.Fatal("failed to read config: ", err)
	}

	grpcServer, err := grpc.NewServer(config.GRPC)
	if err != nil {
		log.Fatal("failed to start grpc server: ", err)
	}
	grpcServer.Start()

	httpServer := http.NewServer(config.HTTP, config.GRPC.Address)
	httpServer.Start(ctx)

	wait := make(chan interface{})
	<-wait
}
