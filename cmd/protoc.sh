#!/bin/bash

# go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
# go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
# go get -u github.com/golang/protobuf/protoc-gen-go
# go get -u sourcegraph.com/sourcegraph/prototools/cmd/protoc-gen-doc

protoc -I/usr/local/include -I ../client \
  -I$GOPATH/src \
  --grpc-gateway_out=logtostderr=true:../client/grpc \
  --go_out=plugins=grpc:../client/grpc \
  --swagger_out=logtostderr=true:. \
  ../client/eventor.proto