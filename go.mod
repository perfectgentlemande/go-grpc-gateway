module bitbucket.org/perfectgentlemande/go-grpc-gateway

go 1.13

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/golang/protobuf v1.4.2
	github.com/grpc-ecosystem/grpc-gateway v1.15.0
	github.com/pkg/errors v0.9.1
	golang.org/x/net v0.0.0-20200923182212-328152dc79b1 // indirect
	golang.org/x/sys v0.0.0-20200923182605-d9f96fdee20d // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/genproto v0.0.0-20200924141100-a14c0a98937d
	google.golang.org/grpc v1.32.0
	google.golang.org/protobuf v1.25.0
	gopkg.in/yaml.v2 v2.3.0
)
