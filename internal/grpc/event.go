package grpc

import (
	"context"

	eventor "bitbucket.org/perfectgentlemande/go-grpc-gateway/client/grpc"
)

func (s *Server) GetEvents(ctx context.Context, _ *eventor.Empty) (*eventor.Events, error) {
	return &eventor.Events{
		Events: []*eventor.Event{{
			ID:   1,
			Name: "Hello 1",
		}, {
			ID:   2,
			Name: "Hello 2",
		}},
	}, nil
}

func (s *Server) AddEvent(ctx context.Context, eventToCreate *eventor.AddEventRequest) (*eventor.Empty, error) {
	return &eventor.Empty{}, nil
}
