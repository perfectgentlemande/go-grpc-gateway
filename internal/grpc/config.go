package grpc

import (
	"log"
	"net"

	eventor "bitbucket.org/perfectgentlemande/go-grpc-gateway/client/grpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type Config struct {
	Address string `yaml:"address"`
}

type Server struct {
	config Config
}

func NewServer(config Config) (*Server, error) {
	return &Server{
		config: config,
	}, nil
}

func (s *Server) Start() {
	go s.startServer()
}

func (s *Server) startServer() {
	listener, err := net.Listen("tcp", s.config.Address)
	if err != nil {
		log.Fatal("failed to listen with address: ", s.config.Address)
	}

	srv := grpc.NewServer()

	reflection.Register(srv)
	eventor.RegisterEventorServer(srv, s)

	log.Println("serving grpc with address: ", s.config.Address)

	err = srv.Serve(listener)
	if err != nil {
		log.Fatal("failed to serve: ", err)
	}
}
