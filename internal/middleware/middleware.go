package middleware

import (
	"net/http"
	"strings"
)

func unprefix(pth, prefix string) string {
	res := strings.TrimPrefix(pth, prefix)
	if !strings.HasPrefix(res, "/") {
		res = "/" + res
	}
	return res
}

func NewUnprefixer(prefix string) func(http.Handler) http.Handler {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			r.URL.Path = unprefix(r.URL.Path, prefix)
			handler.ServeHTTP(w, r)
		})
	}
}
