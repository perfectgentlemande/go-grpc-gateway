package http

import (
	"context"
	"log"
	"net/http"

	eventor "bitbucket.org/perfectgentlemande/go-grpc-gateway/client/grpc"
	"bitbucket.org/perfectgentlemande/go-grpc-gateway/internal/middleware"
	"github.com/go-chi/chi"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc"
)

type Config struct {
	Address string `yaml:"address"`
	Prefix  string `yaml:"prefix"`
}

type Server struct {
	address     string
	prefix      string
	grpcAddress string
}

func NewServer(config Config, grpcAddr string) *Server {
	return &Server{address: config.Address, grpcAddress: grpcAddr, prefix: config.Prefix}
}

func (s *Server) start(ctx context.Context) {
	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	err := eventor.RegisterEventorHandlerFromEndpoint(ctx, mux, s.grpcAddress, opts)
	if err != nil {
		log.Fatal("failed to register eventor server: ", err)
	}
	log.Println("serving http with address: ", s.address)

	unprefixRouter := chi.NewRouter()
	unprefixRouter.Use(middleware.NewUnprefixer(s.prefix))
	unprefixRouter.Mount("/", mux)

	router := chi.NewRouter()
	router.Mount("/", unprefixRouter)

	err = http.ListenAndServe(s.address, router)
	if err != nil {
		log.Fatal("failed to listen and serve: ", err)
	}
}

func (s *Server) Start(ctx context.Context) {
	go s.start(ctx)
}
